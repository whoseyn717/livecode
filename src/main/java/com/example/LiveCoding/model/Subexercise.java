package com.example.LiveCoding.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
@Builder
@Entity
@Data
@NoArgsConstructor
public class Subexercise {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long exerciseId;
        private String taskName;
        private String description;
        private LocalDate lastTime;
        private boolean isActive;

        @ManyToOne(cascade=CascadeType.ALL)
        @JoinColumn(name = "exercise_id", referencedColumnName = "id")
        @ToString.Exclude
        private Exercise exercise;

}
