package com.example.LiveCoding.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Exercise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long exerciseId;
    private String taskName;
    private String description;
    private LocalDate lastTime;
    private boolean isActive;

    @OneToMany(mappedBy = "exercise", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    List<Subexercise>subexercises;


}
