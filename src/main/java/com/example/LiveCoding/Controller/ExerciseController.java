package com.example.LiveCoding.Controller;

import com.example.LiveCoding.model.Exercise;
import com.example.LiveCoding.service.ExerciseService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
    @RestController
    @RequestMapping("/exercises")
    public class ExerciseController {

        @Autowired
        private ExerciseService exerciseService;

        @GetMapping
        public List<Exercise> getAllExercises() {
            return exerciseService.getAllExercises();
        }

        @GetMapping("/{exerciseId}")
        public Exercise getExerciseById(@PathVariable Long exerciseId) {
            return exerciseService.getExerciseById(exerciseId);
        }

        @PostMapping
        public Exercise createExercise(@RequestBody Exercise exercise) {
            return exerciseService.createExercise(exercise);
        }

        @PutMapping("/{exerciseId}")
        public Exercise updateExercise(@PathVariable Long exerciseId, @RequestBody Exercise updatedExercise) {
            return exerciseService.updateExercise(exerciseId,updatedExercise);
        }

        @DeleteMapping("/{exerciseId}")
        public void deleteExercise(@PathVariable Long exerciseId) {
            exerciseService.deleteExercise(exerciseId);
        }
    }











