package com.example.LiveCoding.Repository;

import com.example.LiveCoding.model.Subexercise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubexerciseRepository extends JpaRepository<Subexercise,Long> {
}
