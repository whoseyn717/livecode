package com.example.LiveCoding.Mapper;

import com.example.LiveCoding.dto.ExerciseDto;
import com.example.LiveCoding.dto.SubexerciseDto;
import com.example.LiveCoding.model.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;
@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)

public interface ExerciseMapper {
    ExerciseDto entityToDto(Exercise exercise);
    Exercise dtoToEntity(ExerciseDto exerciseDto);

}
