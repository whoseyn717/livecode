package com.example.LiveCoding.Mapper;
import com.example.LiveCoding.dto.ExerciseDto;
import com.example.LiveCoding.dto.SubexerciseDto;
import com.example.LiveCoding.model.Exercise;
import com.example.LiveCoding.model.Subexercise;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;



@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface SubexerciseMapper {
    SubexerciseDto entityToDto(Subexercise subexercise);
    SubexerciseDto dtoToEntity(SubexerciseDto subexerciseDto);



}




