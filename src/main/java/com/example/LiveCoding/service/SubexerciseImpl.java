package com.example.LiveCoding.service;

import com.example.LiveCoding.Mapper.SubexerciseMapper;
import com.example.LiveCoding.Repository.ExerciseRepository;
import com.example.LiveCoding.Repository.SubexerciseRepository;
import com.example.LiveCoding.dto.SubexerciseDto;
import com.example.LiveCoding.model.Exercise;
import com.example.LiveCoding.model.Subexercise;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@Slf4j
@Primary

public class SubexerciseImpl implements SubexerciseService {

    @Autowired
    private  SubexerciseMapper subtaskMapper;
    private SubexerciseRepository subexerciseRepository;

    public SubexerciseImpl() {
    }

    public List<Subexercise> getAllSubexercises() {
        return subexerciseRepository.findAll();
    }
    @Override
    public Subexercise getSubexerciseById(Long subexerciseId) {
        Optional<Subexercise> subexercise = subexerciseRepository.findById(subexerciseId);
        if (subexercise.isEmpty()) {
            throw new RuntimeException("Subexercise not found with id: "+subexerciseId);
        }
        return subexercise.get();
    }

   public Subexercise createSubExercise(Subexercise subexercise) {
           return subexerciseRepository.save(subexercise); }

    public Subexercise updateSubExercise(Long subexerciseId, Subexercise subexercise) {
        Optional<Subexercise> existingSubexercise = subexerciseRepository.findById(subexerciseId);

        if (existingSubexercise.isPresent()) {
            Subexercise SubexerciseToUpdate = existingSubexercise.get();
            SubexerciseToUpdate.setExerciseId(subexercise.getExerciseId());
            SubexerciseToUpdate.setDescription(subexercise.getDescription());
            SubexerciseToUpdate.setLastTime(subexercise.getLastTime());
            SubexerciseToUpdate.setActive(subexercise.isActive());

            return subexerciseRepository.save(SubexerciseToUpdate);
        } else {
            return null;
        }
    }

    public void deleteSubxercise(Long subexerciseId) {
        subexerciseRepository.deleteById(subexerciseId);

    }
}
