package com.example.LiveCoding.service;

import com.example.LiveCoding.model.Exercise;

import java.util.List;

public interface ExerciseService {


    Exercise getExerciseById(Long exerciseId);
    Exercise updateExercise(Long exerciseId,Exercise exercise);
    Exercise createExercise(Exercise exercise);
    void deleteExercise(Long id);
    List<Exercise> getAllExercises();
}
