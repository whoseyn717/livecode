package com.example.LiveCoding.service;

import com.example.LiveCoding.Repository.ExerciseRepository;
import com.example.LiveCoding.model.Exercise;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExerciseImpl implements ExerciseService {
    @Autowired
    private ExerciseRepository exerciseRepository;

    public List<Exercise> getAllExercises() {
        return exerciseRepository.findAll();
    }
@Override
    public Exercise getExerciseById(Long exerciseId) {
        Optional<Exercise> exercise = exerciseRepository.findById(exerciseId);
        if (exercise.isEmpty()) {
            throw new RuntimeException("Exercise not found with id: "+exerciseId);
        }
        return exercise.get();
    }

    public Exercise createExercise(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }

    public Exercise updateExercise(Long exerciseId, Exercise exercise) {
        Optional<Exercise> existingExercise = exerciseRepository.findById(exerciseId);

        if (existingExercise.isPresent()) {
            Exercise exerciseToUpdate = existingExercise.get();
            exerciseToUpdate.setExerciseId(exercise.getExerciseId());
            exerciseToUpdate.setDescription(exercise.getDescription());
            exerciseToUpdate.setLastTime(exercise.getLastTime());
            exerciseToUpdate.setActive(exercise.isActive());

            return exerciseRepository.save(exerciseToUpdate);
        } else {
            return null;
        }
    }

    public void deleteExercise(Long exerciseId) {
        exerciseRepository.deleteById(exerciseId);

    }

}


