package com.example.LiveCoding.service;

import com.example.LiveCoding.model.Exercise;
import com.example.LiveCoding.model.Subexercise;

import java.util.List;

public interface SubexerciseService {


    Subexercise getSubexerciseById(Long subexerciseId);
    Subexercise updateSubExercise(Long exerciseId,Subexercise subexercise);
    Subexercise createSubExercise(Subexercise subexercise);
    void deleteSubxercise(Long id);
    List<Subexercise> getAllSubexercises();
}
