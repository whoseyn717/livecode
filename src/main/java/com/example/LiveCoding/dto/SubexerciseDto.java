package com.example.LiveCoding.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class SubexerciseDto {
    private Long exerciseId;
    private String taskName;
    private String description;
    private LocalDate lastTime;
    private boolean isActive;

}
